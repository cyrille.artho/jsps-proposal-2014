%form: kiban_ab_form_01-02.tex ; user: kiban_ab_01-02_purpose.tex
%========== S-1-7 基盤研究(A,B)(一般) =========
%===== p. 01-02 研究目的 =============
\section{研究目的}
%watermark: w02_purpose_AB
\newcommand{\研究目的概要}{%
%begin  研究目的概要＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
Today's software has frequent defects, which cause trouble to users and require
maintenance.
\emph{\color{RED}Model-based Testing} can find defects in complex
software systems, but test case generation can be inefficient
because many similar tests are created. \textbf{Smart test generation} will improve this.
%this results in much redundancy and slowly growing coverage.
We will explore extensions of \emph{\color{RED}Adaptive Random Testing},
which can find defects early,
with various coverage measurement strategies and priority settings.
Furthermore, we want to group the results into distinctive tests
by applying
\emph{\color{RED}Test Minimization} and
\emph{\color{RED}Clustering} to automatically group and summarize essential
information for the user, making the tool more effective for software developers.
%end  研究目的概要 ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
}

\newcommand{\研究目的}{%
%begin  研究目的＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{\color{BLUE}\textbf{\ding{192} Scientific background}}

Software testing executes a system under test (SUT). A series of inputs is
fed to the SUT, which responds with a series of outputs. 
Many techniques exist to automate testing, and
to impose some rigor by systematic selection of test data and measurement
of the effectiveness of tests.

\setlength{\columnsep}{16pt}
         \begin{wrapfigure}{r}{4.1cm}
		\vspace{-.5\baselineskip}
		\begin{center}
			\vspace{-\baselineskip}
		        \includegraphics[scale=0.475]{pics/mbt}
		        \caption{\small{Model-based testing.}}
		        \label{fig:mbt}
	         \end{center}
		\vspace{-.5\baselineskip}
         \end{wrapfigure}
%
In {\color{RED}\emph{model-based testing,}}
test cases are derived from a high-level model rather than described
directly in code~\cite{utting1}. From such a model, many differen test
cases can be derived by a test generation tool (see Fig.~\ref{fig:mbt}).

         \begin{wrapfigure}{l}{7.5cm}
		\begin{center}
			\vspace{-\baselineskip}
		        \includegraphics[scale=0.8]{pics/paths}
		        \caption{\small{Current test diversity (red) and potential improvement (blue).}}
		        \label{fig:paths}
	         \end{center}
		\vspace{-2\baselineskip}
         \end{wrapfigure}
%
At AIST, we develop two model-based test tools based on
probabilistic search
\emph{Modbat}~\cite{modbat1} and \emph{\xtool}~\cite{choi1}, which is a
probablistic combinatorial
tool that complements \emph{Calot}~\cite{calot1}.
Modbat takes an extended finite state machine (a graph) and generates
test sequences from that graph. To test combinations of parameters, \xtool\ 
uses weights to generate high-risk test cases early and often.

The \textbf{problem} is that not all system states are hit with the same
probability. For example, if we add data with the same frequency as removing data,
the size of the data stays near zero. Because of this, a pure random search
might miss some defects while generating many similar tests (see Fig.~\ref{fig:paths}).
Our proposed improvements \textbf{increase test diversity} (phase 1, test generation)
and create a shorter, \textbf{distinctive summary} of the results (phase 2, test minimization
and clustering).

\vspace{1.1\baselineskip}

%\vspace{2mm}
{\color{BLUE}\textbf{\ding{193} Goals}}

\textbf{Phase 1:}
To increase test diversity, we want to employ a smart search based on
\emph{\color{RED}Adaptive Random Testing (ART)}.
ART spreads a random search more evenly over the given state space~\cite{chen1}.
The key challenge is the definition of a suitable distance function that
can be computed with low computational overhead. By
integrating the coverage and distance metrics in our tools, we can achieve
a better test diversity while keeping this overhead low.
In the context of a graph-based search like in Modbat, the \emph{bandit method}
can refine ART: It balances the analysis of new states
(exploration) versus promising known states (exploitation).
For \xtool, ongoing work on estimating initial weights based on previous bug reports
will be used to determine the initial settings.

\textbf{Phase 2:}
Currently our tools generate a large set of successful and failing
test cases, without relating test failures to each other. An analysis of failed
tests by a human can be time-consuming; therefore it is desirable to minimize
and summarize failed tests first. We plan to use \emph{\color{RED}test minimization}
methods
such as \emph{delta debugging}~\cite{zeller1} and data mining techniques such as
\emph{\color{RED}clustering}~\cite{everitt1}
to reduce the size of the resulting tests, and group similar tests together.

\vspace{1mm}
{\color{BLUE}\textbf{\ding{194} Scientific characteristic, originality, expected results, impact}}

\vspace{-.5\baselineskip}
\begin{enumerate}
\item \textbf{Test diversity: Distance measures and coverage metrics.}
To adapt ART in a smart search, the following problems have to be solved~\cite{chen1}:
Test executions have to be represented as a directed
graph. However, it is not clear how the distance between states is defined.
It is not practical to compare entire SUTs to determine a distance measure.
A comparison of model states may be feasible for smaller models, especially
if a user or a static analysis determines which subset of variables to use for comparison.
Similar issues exist for sets of parameters, which are not always numerical.\\
We plan to increase test diversity by using path coverage to guide test generation.
As the number of possible paths is usually infinite, it is not clear how to record (partial)
path coverage efficiently, with low memory usage. The right choice may
depend on the type of system.
%\vspace{-.5\baselineskip}
%\item \textbf{Bandit method.} The bandit method requires
%tuning, so the optimal choices depend on the type of problem to be solved.
%A choice may be based on various data ranging from how often a state has already
%been covered by previous tests, what kinds of weight is
%given to certain choices (by the user, by historical data on previously
%found defects, or by analyzing the structure of the SUT), or even by
%run-time data such as warnings emitted by an SUT after a certain action.
\vspace{-.5\baselineskip}
\item \textbf{Distinctive tests: Test minimization and clustering.}
For effective reporting, we plan to adapt existing
test minimization techniques~\cite{zeller1} to our problem setting, which
uses graphs and sets of parameters. Furthermore, we will use the distance
measures defined above in clustering algorithms, to group the failed test
cases and present a concise summary to the user.
\end{enumerate}
\vspace{-.5\baselineskip}

         \begin{wrapfigure}{r}{8.5cm}
			\vspace{-2.5\baselineskip}
		        \hspace{1.2cm}\includegraphics[scale=0.6]{plot/coverage}
			\vspace{.5\baselineskip}
		        \caption{\small{Proposed improvement in path coverage.}}
		        \label{fig:plot}
			\vspace{-.5\baselineskip}
         \end{wrapfigure}

\textbf{\color{RED}Impact:}
In real-life use, test case generation methods still suffer from a low
diversity of generated test cases in large test sets (see Fig.~\ref{fig:plot}).
Our work will provide new insights into many problems
of practical importance. Results will be published
at major international conferences and in journals.
On the practical side, we expect this work to have a major impact
on the effectiveness of model-based testing, making such technologies
commercially much more viable than at present.
	
	\vspace{3mm}
	{\small
	\begin{thebibliography}{99}
		\bibitem{utting1} M.~Utting et al. 「Practical Model-based Testing: A Tools Approach.」Morgan-Kaufmann (2007).\vspace{-1mm}
		\bibitem{modbat1} C.~Artho et al.「Modbat: A Model-based API Tester for Event-driven Systems.」Proc.\ 9th Haifa Verification Conference. Springer LNCS 8244 (2013).\vspace{-1mm}
		\bibitem{choi1} E.~Choi et al.「Priority Integration for Weighted Combinatorial Testing.」Proc.\ IEEE Computer Software and Applications Conf. IEEE (2015)\vspace{-1mm}
		\bibitem{calot1} T.~Kitamura et al.「Test-case Design by Feature Trees.」Proc.\ ISoLA. Springer LNCS 7609 (2012).\vspace{-1mm}
		\bibitem{chen1} T.~Chen et al.「Adaptive Random Testing.」Proc.\ Advances in Computer Science. Springer LNCS 3321 (2004)\vspace{-1mm}
		\bibitem{zeller1} A.~Zeller. 「Why Program Fail.」Morgan Kaufmann, 2009.\vspace{-1mm}
		\bibitem{everitt1} B. Everitt. 「Cluster analysis.」Wiley Series in Probability and Statistics, 2011.\vspace{-1mm}
%		\bibitem{mcts1} H.~Baier et al.「The Power of Forgetting: Improving the Last-Good-Reply Policy in Monte Carlo Go.」IEEE Trans.\ Comp.\ Intell.\ AI Games, 2(4), 2010.\vspace{-1mm}
%		\bibitem{mcts-survey1} C.~Browne et al.「A Survey of Monte Carlo Tree Search Methods.」IEEE Trans.\ Comp.\ Intell.\ AI Games, 4(1), 2012.
	\end{thebibliography}
	}
%end  研究目的 ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
}

%====================================
