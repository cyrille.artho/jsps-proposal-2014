%form: kiban_ab_form_03-05.tex ; user: kiban_ab_03-05_plan.tex
%========== S-1-7 基盤研究(A,B)(一般) =========
%===== p. 03-05 研究計画・方法 =============
\section{研究計画・方法}
%watermark: w08_plan_AB
\newcommand{\研究計画と方法概要}{%
%begin  研究計画と方法概要＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
The initial phase of the project implements \textbf{smart testing}. We will
explore coverage and distance measures to increase test diversity.
The necessary enhancements in Modbat and \xtool{} will be implemented
by Artho and Choi together with a post-doctoral researcher who will be
hired for this project.
In the second and third years, we will implement \textbf{effective reporting} using
test minimization and clustering
of similar failed tests. This will reduce the
burden on the developer who has to analyze the test outcomes.
%end  研究計画と方法概要 ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
}

\newcommand{\研究計画}{%
%begin  研究計画＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

\vspace{1mm}
{\color{BLUE}\textbf{\ding{192} First phase: Test diversity using a smart search (2016)}}

\setlength{\columnsep}{20pt}%
         \begin{wrapfigure}{r}{4.7cm}
		\vspace{-\baselineskip}
			\begin{center}
		        \includegraphics[scale=0.475]{pics/fsm}
		        \caption{\small{A Modbat model with \texttt{start} $\rightarrow$ \texttt{reconfigure} $\rightarrow$ \texttt{shutdown} as a possible path.}}
		        \label{fig:fsm}
	         \end{center}
		\vspace{2\baselineskip}
         \end{wrapfigure}
%
Upon acceptance of the proposal, we will recruit a post-doctoral researcher
for this project (\textbf{WP0}). We expect a post-doctoral researcher to have the necessary
mathematical and software engineering background for this project.

At the beginning, we will analyze various theoretical concepts of path coverage.
Modbat~\cite{modbat} generates test cases using a random walk through the
graph (see Fig.~\ref{fig:fsm}).
The current version tracks state and transition coverage but not path
coverage. Path coverage is difficult because the number of
paths is potentially infinite, so abstraction and compression techniques
are needed to handle large test sets.
For example, in Fig.~\ref{fig:fsm},
infinitely many loops between ``init'' and ``active'', or from ``active''
to itself, are possible.

\begin{wrapfigure}{l}{7.5cm}
\vspace{-1.2\baselineskip}
\begin{center}
{\footnotesize
  \begin{tabular}[t]{l|l} \hline
    \textit{Parameter} & \textit{Value}(\textit{Weight}) \\ \hline%\hline
    \underline{O}S  & \underline{W}in(1), \underline{U}nix(1), \underline{M}ac(2)  \\
    \underline{C}PU & \underline{I}ntel(1), \underline{A}MD(3) \\
    \underline{B}rowser & \underline{I}E(1), \underline{F}irefox(1),  \underline
{S}afari(1), \underline{O}pera(1) \\ 
    \underline{N}et & \underline{W}ifi(2), \underline{L}AN(2), 3\underline{G}(2)
  \\\hline
  \end{tabular}
}
  \caption{\small An example (weighted) SUT model.\label{tab:test-model}}
\vspace{-1.2\baselineskip}
\end{center}
\end{wrapfigure}

In \xtool~\cite{choi}, parameters are represented as sets of weighted values (see
Fig.~\ref{tab:test-model}). Weights are
initially set by a user or %through program analysis, and are
correlated with the \emph{presumed}
importance of a value for testing.
We want to explore adaptive technologies that
balance the exploration of parameters against such static weights and
knowledge gained at run-time (based on error detection rate and code coverage).

\vspace{-.5\baselineskip}
\begin{description}
\item [{WP1.1:}] Define and implement path coverage in Modbat
on a model level. We will experiment with various ways of tracking
partial coverage, for example by tracking only pairs or triples of
states, by compressing self-transitions, etc.
\vspace{-0.5\baselineskip}
\item [{WP1.2:}] Track coverage of parameter-value sets in tests produced
by \xtool. Coverage will include occurrences of system errors, warnings,
and at a later stage code coverage. For code coverage, we plan to leverage
the currently leading Java coverage tool \emph{JaCoCo}
(\url{http://www.eclemma.org/jacoco/}) by extending it so that coverage
can be queried while the system is still running.
\end{description}
\vspace{-.5\baselineskip}

After test coverage is defined and can be measured, we have to adapt the
test generation (search) strategy to take coverage into account:
\vspace{-.5\baselineskip}
\begin{description}
\item [{WP2.1:}] Adaptation of path coverage for test case generation in Modbat.
This will use techniques from adaptive random testing and Monte Carlo Tree Search
to spread the search across more diverse paths. We will also explore ways to
create a persistent representation of the search space, so test case generation
can be resumed from a given point, as is currently the case with Modbat's random
search.
\vspace{-0.5\baselineskip}
\item [{WP2.2:}] Adaptation of error and code coverage for testing with \xtool.
The challenges are similar to WP2.1, except that we deal with sets of parameters,
so graph-based approaches are not applicable here.
\end{description}
\vspace{-.5\baselineskip}

This project requires significant implementation
effort, on complex tool platforms that are using state-of-the-art components
such as the Scala compiler and run-time environment for Modbat's domain-specific
language, and JaCoCo for \xtool, hence the need for a
post-doctoral researcher with both a theoretical and practical background.

\vspace{2mm}
{\color{BLUE}\textbf{\ding{193} Second phase: Effective reporting using test minimization (2017) and clustering (2018)}}

\begin{figure}[t]
\begin{center}
\resizebox{\linewidth}{!}
{\small
\input{../workplan}}
\vspace{-.5\baselineskip}
\caption{\small{Overview of all work packages in this project.}\label{fig:wp}}
\vspace{-1\baselineskip}
\end{center}
\end{figure}

Generated test cases often include extraneous steps that are not essential
for reproducing a failure in software. Chosen parameters are also often larger
or more complex than necessary. An ideal test report includes test cases with
a small number of steps and simple parameters values, such as small numbers
or short text strings.
In year 2, we will address this problem and minimize the tests generated by Modbat.
We will also explore minimizing parameter values generated by \xtool{} where applicable
(e.\,g., for numbers and strings).

\vspace{-.5\baselineskip}
\begin{description}
\item [{WP3.1:}] Adaptation of Zeller's delta debugging algorithm~\cite{zeller}
to Modbat's graph-based test models. In traditional delta debugging, a test
input is minimized; this idea is also applicable to a test sequence. However, in
model-based testing, a test sequence is subject to further constraints (from the
model structure), so the existing algorithm has to be adapted to that setting.
\vspace{-0.5\baselineskip}
\item [{WP3.2:}] Adaptation of value minimization strategies in \xtool. Smaller
values (smaller numbers, shorter text, etc.) are easier to understand for a human
and thus desirable as example inputs. We will leverage and refine existing heuristics
to optimize input values in \xtool.
\end{description}
\vspace{-.5\baselineskip}

% Include choices; include concurrency in search as choices?

% Remove visit to JKU?

Generated test cases often include variants of the same test. A test may fail
with action $a$ or $b$ as the first step, or for values $1$ and $-1$. We will
apply clustering methodologies to group similar tests into classes, so the end
user only needs to consider one test report per class when deciding how to
allocate resources to fix defects. This will greatly improve the productivity
with our tools when software contains multiple defects.
\vspace{-.5\baselineskip}
\begin{description}
\item [{WP4:}] Exploration of clustering for test cases in Modbat and \xtool:
We will distance measurements for generated action sequences (paths) and test
parameters. These distance measurements are then used with clustering algorithms
to group similar test cases together.
\end{description}
\vspace{-.5\baselineskip}

\vspace{-.5\baselineskip}
\begin{description}
\item [{WP5:}] To measure the impact of our research on real software, we will
evaluate our tools on complex real-life software in different domains. We plan
to publish the results at leading software engineering conferences like ASE
(Automated Software Engineering) or ICSE (Int.\ Conf.\ on Software Engineering).
\end{description}
\vspace{-.5\baselineskip}

To evaluate the effectiveness of our methodology, we want to apply
our original and enhanced tools to three problems of increasing complexity.
(1) Java collections (algorithmic data structures)~\cite{modbat2},
(2) a recently published
model of a SAT solver that uses many manually tuned weights~\cite{modbat2},
(3) Apache ZooKeeper, a distributed server for cloud computing.

We will evaluate our work based on the following criteria: (1) defect detection
capability, (2) effectiveness of the defect report. The first criterion covers
how many defects are found in a given time, before and after
our improvements. We will also measure how improved test diversity increases code
coverage, in cases where few or no defects exist in a given system.
The second criterion covers the number of defect reports that
are shown to the end user, and the number of distinct defect types that are
identified. These measures show how useful the tool is for a project manager
that has to decide whether and how to fix newly found defects.

We will also apply our tools in an industrial setting, given the opportunity.

\vspace{2mm}
{\color{BLUE}\textbf{\ding{194} Project organization}}

\setlength{\tabcolsep}{4pt}
\begin{table}[b]
        \begin{center}
	\vspace{-.25\baselineskip}
	{\small
	\begin{tabular}{ll}
	Name&Role\\\hline
	Cyrille Artho	& Principal investigator, technical + scientific lead for Modbat.
\\\hline
	Eun-Hye Choi	& Collaborator (研究分担者), technical + scientific lead for \xtool.
\\
	Takashi Kitamura	& Collaborator (研究分担者), tool integration and evaluation.
\\
	Yoriyuki Yamagata	& Collaborator (研究分担者), publicity, scientific adviser.\\
	Akira Mori	& Collaborator (連携分担者), scientific adviser.\\
	\end{tabular}
	}
	\vspace{-.5\baselineskip}
        \caption{Project organization.\vspace{-\baselineskip}}
        \label{fig:org}
        \end{center}
	\vspace{-.5\baselineskip}
\end{table}

The project has Artho as the Principal Investigator. He has many
years of experience with a broad range of verification technologies,
including model-based testing. He also successfully engineered many
program analysis tools, most of them in collaboration with other researchers
and post-docs.

%Artho will lead this project and hire one post-doc, for who most of the
%project funding is allocated. The post-doc will develop and test
%the software tools.
%Several members of the research group where Artho works will also
%contribute:
Choi has recently created a tool to generate test cases prioritized by
weight~\cite{choi}, which can be implemented using a probablistic search.
Kitamura is the principal investigator
of a JST-funded project to develop a commercial version of Calot, which
is related to \xtool. Finally,
Yamagata and Mori have collaborated on other recent work in testing
(see Table~\ref{fig:org}).

\vspace{2mm}
{\color{BLUE}\textbf{\ding{195} Possible adaptations/adjustments}}

Complex path coverage metrics and test coverage (JaCoCo) integration
may be postponed if the project falls behind schedule or no suitable
research assistant can be found. Likewise, clustering may be implemented
as a proof of concept or as a working tool, depending on progress.

	\vspace{3mm}
	{\small
	\begin{thebibliography}{99}
		\bibitem{modbat} C.~Artho et al.「Modbat: A Model-based API Tester for Event-driven Systems.」Proc.\ 9th Haifa Verification Conference. Springer LNCS 8244 (2013).\vspace{-1mm}
		\bibitem{modbat2} C.~Artho et al.「Model-based Testing of Stateful APIs with Modbat.」Proc. Int.\ Conf.\ on Automated Software Engineering. IEEE, to appear (2015).\vspace{-1mm}
		\bibitem{choi} E.~Choi et al.「Priority Integration for Weighted Combinatorial Testing.」Proc.\ IEEE Computer Software and Applications Conf. IEEE (2015)\vspace{-1mm}
		\bibitem{zeller} A.~Zeller. 「Why Program Fail.」Morgan Kaufmann, 2009.\vspace{-1mm}
	\end{thebibliography}
	}

%\begin{center}
%{\Large
%This page intentionally left blank.}
%\end{center}

%{\small While this project has the structure of a kiban-S (exploratory) project,
%we will consider submitting a larger proposal such as kiban-B to JSPS.}
%end  研究計画 ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
}

