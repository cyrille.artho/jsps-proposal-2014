set terminal postscript eps enhanced color "Helvetica" 32
set grid

set out "coverage.ps"
set xlabel "Tests"
set ylabel "Diversity: Unique paths"
set xrange [0:*]
set xtics 25000
set yrange [0:25000]
#set ytics 5
unset y2label
unset y2tics
#set format y '%2.0f%%'
set key top left

f(x) = x/1000
plot \
"server-coverage.data" using 1:4 title "Case 1" axes x1y1 with points lw 2, \
"client-coverage.data" using 1:4 title "Case 2" axes x1y1 with points lw 2, \
"client-coverage-sel.data" using 1:4 title "Case 3" axes x1y1 with points lw 2 lt 4, \
"server-coverage.data" using 1:($4*3) title "Proposed" axes x1y1 with points lw 2 lt 6

#f(x) title "Ideal case (100 %)" with lines ls 1 linecolor rgbcolor "red" lw 5
